with open('input.txt', 'r') as file:
  input = file.read()

start_floor = 0
for i in range(len(input)):  
  if input[i] == '(':
    start_floor += 1
  else:
    start_floor -= 1
  if start_floor == -1:
    start_floor = i +1
    break

print(start_floor)