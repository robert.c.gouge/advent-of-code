filename = 'input.txt'

class House:
  def __init__(self,x,y):
    self.x = x
    self.y = y
    self.timesVisited = 0
  def visit(self):
    self.timesVisited += 1
  def __str__(self):
    return f"House is at [{self.x}, {self.y}]. Visited: {self.timesVisited} times."

startingHouse = House(0,0)
startingHouse.visit()
housesVisited = []
housesVisited.append(startingHouse)
with open(filename, 'r') as file:
  directions = file.read()
  currentX = startingHouse.x
  currentY = startingHouse.y
  print(f"{currentX},{currentY}")
  for direction in directions:
    print(f"Moving: {direction}")
    match direction:
      case '>':
        currentX += 1
        currentY = currentY
        print(f"We are at [{currentX}, {currentY}], checking if this house exists")
        if [house for house in housesVisited if house.x == currentX and house.y == currentY]:
          print("Found!")
          currentHouse = [house for house in housesVisited if house.x == currentX and house.y == currentY][0]
          currentHouse.visit()
        else:
          print("Not Found")
          currentHouse = House(currentX, currentY)
          currentHouse.visit()
          housesVisited.append(currentHouse)
      case '<':
        currentX -= 1
        currentY = currentY
        print(f"We are at [{currentX}, {currentY}], checking if this house exists")
        if [house for house in housesVisited if house.x == currentX and house.y == currentY]:
          print("Found!")
          currentHouse = [house for house in housesVisited if house.x == currentX and house.y == currentY][0]
          currentHouse.visit()
        else:
          print("Not Found")
          currentHouse = House(currentX, currentY)
          currentHouse.visit()
          housesVisited.append(currentHouse)
      case '^':
        currentX = currentX
        currentY += 1
        print(f"We are at [{currentX}, {currentY}], checking if this house exists")
        if [house for house in housesVisited if house.x == currentX and house.y == currentY]:
          print("Found!")
          currentHouse = [house for house in housesVisited if house.x == currentX and house.y == currentY][0]
          currentHouse.visit()
        else:
          print("Not Found")
          currentHouse = House(currentX, currentY)
          currentHouse.visit()
          housesVisited.append(currentHouse)
      case 'v':
        currentX = currentX
        currentY -= 1
        print(f"We are at [{currentX}, {currentY}], checking if this house exists")
        if [house for house in housesVisited if house.x == currentX and house.y == currentY]:
          print("Found!")
          currentHouse = [house for house in housesVisited if house.x == currentX and house.y == currentY][0]
          currentHouse.visit()
        else:
          print("Not Found")
          currentHouse = House(currentX, currentY)
          currentHouse.visit()
          housesVisited.append(currentHouse)
print(len(housesVisited))