def getWrappingPaperNeeded(line):
  l,w,h = line.split('x')
  l = int(l)
  w = int(w)
  h = int(h)
  side1 = 2 * l * w
  side2 = 2 * w * h
  side3 = 2 * h * l
  sides = [side1, side2, side3]
  sides.sort()
  surfaceArea = side1 + side2 + side3
  print(f"Length: {l}, Width: {w}, Height: {h}, Surface Area: {surfaceArea}, Smallest Side: {sides[0]}, Total Needed: {surfaceArea + (sides[0] / 2)}")
  return surfaceArea + (sides[0] / 2)
  
def getRibbonNeeded(line):
  l,w,h = line.split('x')
  l = int(l)
  w = int(w)
  h = int(h)
  dimensions = [l,w,h]
  dimensions.sort()
  wrap = dimensions[0] + dimensions[0] + dimensions[1] + dimensions[1]
  volume = l * w * h
  print(f"Wrap Ribbon: {wrap}, Bow Ribbon: {volume}, Needed {wrap + volume}")
  return wrap + volume

#squareFeetNeeded = 0
ribbonNeeded = 0
with open('input.txt', 'r') as file:
  for line in file:
    line = line.strip()
    #squareFeetNeeded += getWrappingPaperNeeded(line)
    ribbonNeeded += getRibbonNeeded(line)

#print(squareFeetNeeded)
print(ribbonNeeded)